package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AgendaDbHelper extends SQLiteOpenHelper {
    private static  final String TEXT_TYPE= " TEXT";
    private static  final String INTEGER_TYPE= " INTEGER";
    private static  final String COMMA= " ,";
    private static  final String SQL_CREATE_CONTACTO= " CREATE TABLE " +
            DefinirTabla.Contactos.TABLE_NAME + "(" +
            DefinirTabla.Contactos._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Contactos.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.Contactos.DOMICILIO + TEXT_TYPE + COMMA +
            DefinirTabla.Contactos.TELEFONO1 + TEXT_TYPE + COMMA +
            DefinirTabla.Contactos.TELEFONO2 + TEXT_TYPE + COMMA +
            DefinirTabla.Contactos.NOTAS + TEXT_TYPE + COMMA +
            DefinirTabla.Contactos.FAVORITO + INTEGER_TYPE + ")";
    public static  final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Contactos.TABLE_NAME;
    private static final int DATABASE_VERSION= 1;
    private static final String DATABASE_NAME= "agenda.db";

    public AgendaDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_CONTACTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_CONTACTO);
        onCreate(sqLiteDatabase);
    }
}
